# Dériveur lesté

La version dériveur lesté est équipée d'un saumon de quille en fonte et d'une dérive en acier.

## Dérive

Il semble exister deux versions de celle-ci, dont voici les plans:

### Version 1

![Plan de la dérive version 1](images/plan_derive_attalia_v1.jpg)


### Version 2

![Plan de la dérive version 2](images/plan_derive_attalia_v2.jpg)
