# Introduction

Ce site vise à rassembler ce qui existe de documentation technique concernant l'Attalia de Jeanneau.

Ce n'est pas un site officiel du constructeur!

J'ai acheté un Attalia, mon premier bateau, en Novembre 2022, et je me suis vite rendu compte qu'il est difficile de
trouver certaines infos techniques. Ce site pourra donc servir à les regrouper, en espérant qu'il soit utile à d'autres
propriétaires!

Il existait un site `Attaliamis` qui semble avoir disparu, mais dont une copie est accessible sur
[WaybackMachine](https://web.archive.org/web/20081112095711/http://attalia.fr/index.html). J'espère juste rendre le contenu
un peu plus facile d'accès. J'ai repris une partie du contenu de ce site, en essayant de le réorganiser un peu et en
ajoutant un peu de contexte si possible.

## Quelques remarques

Je ne suis pas un expert en la matière, je viens d'acquérir mon premier bateau et j'ai beaucoup à apprendre. Je ferai
forcement des erreurs, n'hésitez pas à me les indiquer, et je ferai de mon mieux pour les corriger.

Si vous avez des infos ou documents qui ne figurent pas sur le site mais qui pourraient servir à d'autres, envoyez les moi!

S'il y a des documents sur le site qui enfreignent vos droits d'auteur, contactez-moi pour les enlever.
